import glob
import os
from os.path import basename
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "avozrouca.settings")

import django
django.setup()

from pages.models import Page, Section, Image
from django.core.files import File


sp = Section(title="São Paulo", slug="sp")
bh = Section(title="Belo Horizonte", slug="bh")
ead = Section(title="EAD", slug="ead")

sp.save()
bh.save()
ead.save()

for section_obj, section_str, section_len in (sp, "sp", 20), (bh, "bh", 2):
    # Todo: add order
    for i in range(section_len, 0, -1):
        with open(f"old/pdfs/{section_str}{i}.pdf", "rb") as f:
            page = Page(
                title=f"#{i}",
                slug=f"{i}",
                section=section_obj,
                pdf=File(f, name=f"{section_str}{i}.pdf"),
            )
            page.save()

        for filename in sorted(glob.glob(f"old/images/{section_str}{i}-*.jpg")):
            with open(filename, "rb") as f:
                im = Image(
                    image=File(f, name=basename(filename)),
                    page=page
                )
                im.save()
