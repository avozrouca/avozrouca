import re
from django.conf import settings
from django.http import Http404
from .models import Page, Section
from django.shortcuts import render, get_object_or_404, redirect

def build_css_content():
    try:
        with open("pages/static/pages/style.css", "r") as f:
            ret = f.read()
            ret = re.sub(
                r'url\("([^"]+)"\)',
                fr'url("{settings.STATIC_URL}pages/\1")',
                ret
            )
    except FileNotFoundError:
        ret = None
    return ret

def build_default_context():
    return {"css_content": css_content}

def page_cover_images(page):
    if page.cover_image:
        return [page.cover_image,]

    images = list(map(lambda x: x.image, page.image_set.all()))
    return images[:min(2, len(images))]


def section_cover_images(section):
    try:
        page = section.page_set.all()[0]
        return page_cover_images(page)
    except IndexError:
        return []


def index(request):
    context = build_default_context()
    context["sections"] = list(map(
        lambda s:
            (s, section_cover_images(s)),
        Section.objects.all(),
    ))

    return render(request, "pages/index.html", context)

def section(request, slug):
    context = build_default_context()
    context["section"] = get_object_or_404(Section, slug=slug)
    context["pages"] = list(map(
        lambda p:
            (p, page_cover_images(p)),
        Page.objects.filter(section=context["section"])
    ))


    return render(request, "pages/section.html", context)

def page(request, section, slug):
    page = get_object_or_404(
        Page,
        slug=slug,
        section__slug=section
    )

    if section != page.section.slug:
        return Http404

    if page.redirect:
        return redirect(page.redirect)

    context = build_default_context()
    context["page"] = page
    context["images"] = page.image_set.all()

    return render(request, "pages/page.html", context)

legacy_table = {
    "index": ("index", {}),
    "bh_index": ("section", {"slug": "bh"}),
    "sp_index": ("section", {"slug": "sp"}),
    "brodo": ("page", {"section": "ead", "slug": "e-dai-ead" }),
    # "guia": ("page", {"section": "sp", "slug": "guia"}
    "mg1": ("page", {"section": "bh", "slug": "1"}),
    "bh1": ("page", {"section": "bh", "slug": "1"}),
    "bh2": ("page", {"section": "bh", "slug": "2"}),
    "sp1": ("page", {"section": "sp", "slug": "1"}),
    "sp2": ("page", {"section": "sp", "slug": "2"}),
    "sp3": ("page", {"section": "sp", "slug": "3"}),
    "sp4": ("page", {"section": "sp", "slug": "4"}),
    "sp5": ("page", {"section": "sp", "slug": "5"}),
    "sp6": ("page", {"section": "sp", "slug": "6"}),
    "sp7": ("page", {"section": "sp", "slug": "7"}),
    "sp8": ("page", {"section": "sp", "slug": "8"}),
    "sp9": ("page", {"section": "sp", "slug": "9"}),
    "sp10": ("page", {"section": "sp", "slug": "10"}),
    "sp11": ("page", {"section": "sp", "slug": "11"}),
    "sp12": ("page", {"section": "sp", "slug": "12"}),
    "sp13": ("page", {"section": "sp", "slug": "13"}),
    "sp14": ("page", {"section": "sp", "slug": "14"}),
    "sp15": ("page", {"section": "sp", "slug": "15"}),
    "sp16": ("page", {"section": "sp", "slug": "16"}),
    "sp17": ("page", {"section": "sp", "slug": "17"}),
    "sp18": ("page", {"section": "sp", "slug": "18"}),
    "sp19": ("page", {"section": "sp", "slug": "19"}),
    "sp20": ("page", {"section": "sp", "slug": "20"}),
}

def legacy(request, path):
    print(path)
    view, args = legacy_table[path]
    return redirect(view, **args)

css_content = build_css_content()

