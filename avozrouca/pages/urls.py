from django.urls import path, re_path

from . import views

legacy_pattern = rf"^(?P<path>{'|'.join(views.legacy_table.keys())})\.html/?$"

urlpatterns = [
    re_path(legacy_pattern, views.legacy, name="legacy"),
    path("", views.index, name="index"),
    path("<str:slug>/", views.section, name="section"),
    path("<str:section>/<str:slug>/", views.page, name="page"),
]
