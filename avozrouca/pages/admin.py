from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin, SortableTabularInline

from .models import Page, Section, Image

class ImageInline(SortableTabularInline):
    model = Image
    extra = 1

@admin.register(Page)
class PageAdmin(SortableAdminMixin, admin.ModelAdmin):
    inlines = [ImageInline]
    prepopulated_fields = {"slug": ("title", )}

@admin.register(Section)
class SectionAdmin(SortableAdminMixin, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title", )}
