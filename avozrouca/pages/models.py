from django.db import models
from django.core.exceptions import ValidationError
from ckeditor_uploader.fields import RichTextUploadingField


class Section(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["order"]

class Page(models.Model):
    title = models.CharField(max_length=400)
    section = models.ForeignKey(Section, on_delete=models.RESTRICT)
    show_title = models.BooleanField(default=True)
    pdf = models.FileField(upload_to="arquivo", null=True, blank=True)
    slug = models.SlugField()
    cover_image = models.ImageField(upload_to="diario", null=True, blank=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    text = RichTextUploadingField(null=True, blank=True)
    redirect = models.URLField(null=True, blank=True)

    class Meta:
        ordering = ["order"]
        constraints = [
            models.UniqueConstraint(
                fields=["title", "section"],
                name="unique slug and section"
            )
        ]

    def __str__(self):
        return f"{self.section} | {self.title}"

class Image(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="diario")
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ["order"]


